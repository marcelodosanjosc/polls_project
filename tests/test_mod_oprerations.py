from polls_project.math_operations.mod_operations import ModOperation

def test_mod_10_3_should_be_1():
    # given
    a = 10
    b = 3
    expected = 1
    mod_object = ModOperation()
    # when
    result = mod_object.mod(a, b)
    # then
    assert result == expected


