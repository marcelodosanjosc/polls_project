from polls_project.math_operations.sum_operation import SumOperation


def test_sum_1_2_should_be_3():
    # given
    a = 1
    b = 2
    expected = 3
    sum_object = SumOperation()
    # when
    result = sum_object.sum(a, b)
    # then
    assert result == expected
